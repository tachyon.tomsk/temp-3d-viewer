<!DOCTYPE html>
<html lang="ru">
<head>
    <title>3D Viewer</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link type="text/css" rel="stylesheet" href="main.css">
</head>
<body>
<script type="importmap">
			{
				"imports": {
					"three": "./three/build/three.module.js"
				}
			}
</script>

<script type="module">
    import * as THREE from 'three';
    import { OrbitControls } from './three/examples/jsm/controls/OrbitControls.js';
    import { FBXLoader } from './three/examples/jsm/loaders/FBXLoader.js';
    import {GLTFLoader} from "./three/examples/jsm/loaders/GLTFLoader.js";
    import {RGBELoader} from "./three/examples/jsm/loaders/RGBELoader.js";

    let camera, scene, renderer;
    let ssaaRenderPassP, copyPass, composer;
    const enableShadow = false;

    init();
    animate();

    function init() {

        const container = document.createElement( 'div' );
        document.body.appendChild( container );
        camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.1, 1000 );
        camera.position.set( 1, 1, 1 );
        scene = new THREE.Scene();
        const hemiLight = new THREE.AmbientLight( 0xFFFFFF );
        hemiLight.position.set( 0, 5, 0 );
        scene.add( hemiLight );

        const pointLight = new THREE.PointLight( 0xFFFFFF, 5, 0 );
        pointLight.position.set( -2, 3, -1 );
        if (enableShadow) {
            pointLight.castShadow = true;
            pointLight.shadow.mapSize.width = 4096;
            pointLight.shadow.mapSize.height = 4096;
            pointLight.shadow.bias = 0.0001;
            pointLight.shadow.radius = 8;
            pointLight.shadow.camera.near = 0.01;
            pointLight.shadow.camera.far = 60000;
            pointLight.shadowDarkness = 0.5;

            const geometry = new THREE.PlaneGeometry( 2000, 2000 );
            geometry.rotateX( - Math.PI / 2 );

            const material = new THREE.ShadowMaterial();
            material.opacity = 0.2;
            const plane = new THREE.Mesh( geometry, material );
            plane.position.y = 0;
            plane.receiveShadow = true;
            scene.add( plane );
        }
        scene.add( pointLight );
        // const dirLight = new THREE.AmbientLight( 0xFFFFFF );
        // dirLight.position.set( 0, 1, 0 );
        // scene.add( dirLight );
        // scene.add(new THREE.AxesHelper(5))




        renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true  });
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );
        renderer.setClearColor(0x000000, 0);
        renderer.autoClear = true;
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        renderer.setClearAlpha(0.0);
        renderer.toneMapping = THREE.ReinhardToneMapping;
        // renderer.outputEncoding = THREE.sRGBEncoding;

        container.appendChild( renderer.domElement );
        const controls = new OrbitControls( camera, renderer.domElement );
        controls.target.set( 0, 0.5, 0 );
        controls.update();
        window.addEventListener( 'resize', onWindowResize );



        let pmremGenerator = new THREE.PMREMGenerator(renderer);
        pmremGenerator.compileEquirectangularShader();

        const envmapPromise = new Promise((resolve, reject) => {
            new RGBELoader()
                // .setDataType(THREE.UnsignedByteType)
                // .setPath('./')
                .load('environment2.hdr', (texture) => {
                    resolve(pmremGenerator.fromEquirectangular(texture).texture)
                });
        }).then((envMap) => {
            scene.environment = envMap;
            // scene.background = envMap;
        })

        const applyEnvMap = ()=>{
            scene.traverse(async function (node) {
                if (node.isMesh) {
                    if (enableShadow)
                        node.castShadow = true;
                    if (node.material && (node.material.isMeshStandardMaterial ||
                        (node.material.isShaderMaterial && node.material.envMap !== undefined))) {
                        node.material.envMap = await envmapPromise;
                        node.material.envMapIntensity = 5.0;
                    }
                }
            });
        }



        // model
        const loader = new GLTFLoader();
        loader.load( 'scene2.glb', function ( object ) {
            scene.add( object.scene );
            applyEnvMap();
        } );

    }

    function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );
        composer.setSize( window.innerWidth, window.innerHeight );
    }

    function animate() {
        requestAnimationFrame( animate );
        renderer.render( scene, camera );
    }

</script>

</body>
</html>
